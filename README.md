## Sailfish OS packaging for mbedTLS

Mbed TLS is a C library that implements cryptographic primitives, X.509
certificate manipulation and the SSL/TLS and DTLS protocols. Its small code
footprint makes it suitable for embedded systems.

NOTE: this package uses a custom configuration to make the library smaller,
more performant and potentially more secure. Noteworthy things that are
disabled are PSA, SHA384, PSK support and DTLS (encrypted UDP).  If one of
these configurations breaks your app let us know, so we can enable it again.

