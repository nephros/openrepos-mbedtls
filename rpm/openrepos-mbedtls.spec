# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       openrepos-mbedtls

# >> macros
# << macros
%define upstream_name mbedtls
%define mbedtls_config_enable MBEDTLS_THREADING_C MBEDTLS_THREADING_PTHREAD MBEDTLS_HAVEGE_C MBEDTLS_DEPRECATED_WARNING MBEDTLS_AES_ROM_TABLES MBEDTLS_SHA512_NO_SHA384
%define mbedtls_config_disable MBEDTLS_DEBUG_C MBEDTLS_BLOWFISH_C MBEDTLS_ARC4_C MBEDTLS_SSL_PROTO_DTLS MBEDTLS_SSL_DTLS_ANTI_REPLAY MBEDTLS_SSL_DTLS_HELLO_VERIFY MBEDTLS_SSL_DTLS_CLIENT_PORT_REUSE MBEDTLS_SSL_DTLS_BADMAC_LIMIT MBEDTLS_SSL_PROTO_TLS1 MBEDTLS_SSL_CBC_RECORD_SPLITTING MBEDTLS_SSL_KEEP_PEER_CERTIFICATE MBEDTLS_KEY_EXCHANGE_PSK_ENABLED MBEDTLS_KEY_EXCHANGE_DHE_PSK_ENABLED MBEDTLS_KEY_EXCHANGE_ECDHE_PSK_ENABLED MBEDTLS_KEY_EXCHANGE_RSA_PSK_ENABLED

Summary:    An open source, portable, easy to use, readable and flexible SSL library
Version:    2.27.0
Release:    1
Group:      System/Libraries
License:    Apache-2.0
URL:        https://github.com/ARMmbed/mbedtls
Source0:    %{name}-%{version}.tar.gz
Source100:  openrepos-mbedtls.yaml
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires:  cmake >= 2.8.12
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  sed

%description
%{summary}.

%package tools
Summary:    Executable utilities for %{name}
Group:      System
Requires:   %{name} = %{version}-%{release}

%description tools
%{summary}.

%package devel
Summary:    Development files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
Development files for %{name}.

%prep
%setup -q -n %{upstream_name}-%{version}

# >> setup
# << setup

%build
# >> build pre
for f in %{mbedtls_config_enable}; do
sed -i -e "s:^//#define ${f}$\b:#define ${f}:" include/mbedtls/config.h
done
for f in %{mbedtls_config_disable}; do
sed -i -e "s:^#define ${f}$://#define ${f}:" include/mbedtls/config.h
done
%cmake \
-DENABLE_PROGRAMS=true \
-DUSE_STATIC_MBEDTLS_LIBRARY=OFF \
-DUSE_SHARED_MBEDTLS_LIBRARY=ON \
-DENABLE_TESTING=OFF \
-DENABLE_ZLIB_SUPPORT=no \
-DMBEDTLS_FATAL_WARNINGS=OFF \
.
# << build pre


make %{?_smp_mflags}

# >> build post
#make %%{?_smp_mflags} SHARED=true no_test
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%make_install

# >> install post
# programs are installed in /usr/bin, lets move them away
install -d %{buildroot}/%{_libexecdir}/%{name}
mv %{buildroot}/%{_bindir}/* %{buildroot}/%{_libexecdir}/%{name}/
# << install post

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libmbedcrypto.so*
%{_libdir}/libmbedtls.so*
%{_libdir}/libmbedx509.so*
# >> files
# << files

%files tools
%defattr(-,root,root,-)
%{_libexecdir}/%{name}*
# >> files tools
# << files tools

%files devel
%defattr(-,root,root,-)
# >> files devel
# AutoSubPackages does not pick up these:
%{_includedir}/%{upstream_name}/*
%{_includedir}/psa/*
# << files devel
